﻿using System.Linq;

namespace Database.Services
{
    public interface IService<TEntity> where TEntity : class, new()
    {
        IQueryable<TEntity> GetAll();

        TEntity GetById(int id);

        TEntity Add(TEntity entity);

        TEntity Update(TEntity entity);

        void Remove(TEntity entity);
    }
}