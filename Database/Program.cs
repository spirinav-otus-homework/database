﻿using System;
using System.Collections.Generic;
using ConsoleTableExt;
using Database.Controllers;

namespace Database
{
    class Program
    {
        // ReSharper disable once UnusedParameter.Local
        static void Main(string[] args)
        {
            new ConsoleController().Run();
        }

        private static void Output(string output, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(output);
            Console.ResetColor();
        }

        public static void MessageOutput(string message)
        {
            Output(message, ConsoleColor.DarkYellow);
        }

        public static void ErrorOutput(string error)
        {
            Output(error, ConsoleColor.DarkRed);
        }

        public static void ResultOutput(string result)
        {
            Output(result, ConsoleColor.DarkGreen);
        }

        public static void ResultOutput<T>(List<T> result, string name) where T : class
        {
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            ConsoleTableBuilder
                .From(result)
                .WithTitle(name, TextAligntment.Left)
                .WithFormat(ConsoleTableBuilderFormat.MarkDown)
                .ExportAndWriteLine();
            Console.ResetColor();
        }

        public static string Input()
        {
            return Console.ReadLine();
        }

        public static void Wait()
        {
            Console.ReadLine();
        }
    }
}