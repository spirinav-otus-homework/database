﻿using Database.Data.Models;
using Database.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Database
{
    public class SberbankDbContextFactory : IDesignTimeDbContextFactory<SberbankDbContext>
    {
        public SberbankDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<SberbankDbContext>();
            optionsBuilder.UseNpgsql(ConfigHelper.GetConnectionString());
            return new SberbankDbContext(optionsBuilder.Options);
        }
    }
}