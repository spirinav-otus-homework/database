﻿using System.Collections.Generic;
using System.Linq;
using Database.Services;

namespace Database.Controllers
{
    public class Controller<TEntity> where TEntity : class, new()
    {
        private readonly IService<TEntity> _service;

        public Controller(IService<TEntity> service)
        {
            _service = service;
        }

        public List<TEntity> GetAll()
        {
            return _service.GetAll().ToList();
        }

        public TEntity GetById(int id)
        {
            return _service.GetById(id);
        }

        public TEntity Add(TEntity entity)
        {
            return _service.Add(entity);
        }

        public TEntity Update(TEntity entity)
        {
            return _service.Update(entity);
        }

        public void Remove(TEntity entity)
        {
            _service.Remove(entity);
        }
    }
}