﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using Database.Data.Models;
using Database.Data.Repositories;
using Database.Helpers;
using Database.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Database.Controllers
{
    public class ConsoleController
    {
        private ServiceProvider _serviceProvider;

        #region MessageTexts

        private const string ConnectionStringNotDefinedText = "Connection string is not defined.";
        private const string DatabaseIsReadyText = "Database is ready to work.";

        private const string TableMenuText =
            "\nSelect table:\n[1] - customers\n[2] - accounts\n[3] - cards\n[4] - exit\n\nYour choice:";

        private const string ActionMenuText =
            "\nSelect action for {0}s:\n[1] - show all {0}s\n[2] - add new {0}\n[3] - update {0}\n[4] - remove {0}\n[5] - return\n\nYour choice:";

        private const string IncorrectInputText = "\nIncorrect input. Try again.";
        private const string IncorrectInputMessageText = "\nIncorrect input.\nMessage: {0}\nTry again.";
        private const string EntityNotExistsText = "\nSpecified {0} doesn't exists.";
        private const string ValueCannotBeEmptyText = "Value cannot be empty.";
        private const string ValueCannotBeGreaterThanText = "Value length cannot be greater than {0}.";

        private const string EnterValueText = "\nEnter {0} (r - return):";
        private const string SuccessfullyAddedText = "\nSpecified {0} has been successfully added.";

        private const string EnterIdForUpdateText = "\nEnter {0} id for update (r - return):";
        private const string SuccessfullyUpdatedText = "\nSpecified {0} has been successfully updated.";

        private const string EnterIdForRemoveText = "\nEnter {0} id for remove (r - return):";
        private const string SuccessfullyRemovedText = "\nSpecified {0} has been successfully removed.";

        #endregion

        public void Run()
        {
            var connectionString = ConfigHelper.GetConnectionString();
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                Program.ErrorOutput(ConnectionStringNotDefinedText);
                Program.Wait();
                return;
            }

            InitDatabase(connectionString);
            ConfigureServices(connectionString);
            ShowTableMenu();
        }

        private void InitDatabase(string connectionString)
        {
            var optionsBuilder = new DbContextOptionsBuilder<SberbankDbContext>();
            var options = optionsBuilder.UseNpgsql(connectionString).Options;

            using var db = new SberbankDbContext(options);
            if (db.Database.GetPendingMigrations().Any())
            {
                db.Database.Migrate();
            }

            Program.MessageOutput(DatabaseIsReadyText);
        }

        private void ConfigureServices(string connectionString)
        {
            _serviceProvider = new ServiceCollection()
                .AddDbContext<SberbankDbContext>(option => option.UseNpgsql(connectionString))
                .AddTransient(typeof(IRepository<>), typeof(Repository<>))
                .AddTransient(typeof(IService<>), typeof(Service<>))
                .BuildServiceProvider();
        }

        private void ShowTableMenu()
        {
            while (true)
            {
                Program.MessageOutput(TableMenuText);
                switch (Program.Input())
                {
                    case "1":
                        ShowActionMenu<Customer>();
                        break;
                    case "2":
                        ShowActionMenu<Account>();
                        break;
                    case "3":
                        ShowActionMenu<Card>();
                        break;
                    case "4":
                        return;
                    default:
                        Program.ErrorOutput(IncorrectInputText);
                        break;
                }
            }
        }

        private string GetEntityName<TEntity>() where TEntity : class => typeof(TEntity).Name.ToLower();

        private string TextFormat<TEntity>(string text) where TEntity : class
        {
            return string.Format(text, GetEntityName<TEntity>());
        }

        private void ShowActionMenu<TEntity>() where TEntity : class, new()
        {
            while (true)
            {
                Program.MessageOutput(TextFormat<TEntity>(ActionMenuText));

                switch (Program.Input())
                {
                    case "1":
                        OutputAll<TEntity>();
                        break;
                    case "2":
                        Add<TEntity>();
                        break;
                    case "3":
                        Update<TEntity>();
                        break;
                    case "4":
                        Remove<TEntity>();
                        break;
                    case "5":
                        return;
                    default:
                        Program.ErrorOutput(IncorrectInputText);
                        break;
                }
            }
        }

        private void Output<TEntity>(List<TEntity> entities) where TEntity : class, new()
        {
            if (typeof(TEntity) == typeof(Customer))
            {
                Program.ResultOutput(
                    entities.Cast<Customer>()
                        .Select(x => new {x.Id, x.FirstName, x.MiddleName, x.LastName, x.Email, x.Phone})
                        .OrderBy(x => x.Id)
                        .ToList(),
                    GetEntityName<TEntity>() + "s");
            }

            if (typeof(TEntity) == typeof(Account))
            {
                Program.ResultOutput(
                    entities.Cast<Account>()
                        .Select(x => new {x.Id, x.CustomerId, x.AccountNumber, x.OpeningDate, x.ClosingDate, x.Balance})
                        .OrderBy(x => x.Id)
                        .ToList(),
                    GetEntityName<TEntity>() + "s");
            }

            if (typeof(TEntity) == typeof(Card))
            {
                Program.ResultOutput(
                    entities.Cast<Card>()
                        .Select(x => new {x.Id, x.AccountId, x.CardNumber, x.StartDate, x.EndDate, x.IsBlocked})
                        .OrderBy(x => x.Id)
                        .ToList(),
                    GetEntityName<TEntity>() + "s");
            }
        }

        private void OutputAll<TEntity>() where TEntity : class, new()
        {
            var controller = new Controller<TEntity>(_serviceProvider.GetService<IService<TEntity>>());
            Output(controller.GetAll());
        }

        private void OutputSingle<TEntity>(TEntity entity) where TEntity : class, new()
        {
            Output(new List<TEntity> {entity});
        }

        private TEntity FillEntity<TEntity>(TEntity entity, bool isNew) where TEntity : class
        {
            var properties = typeof(TEntity)
                .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .OrderBy(x => x.MetadataToken)
                .ToList();

            foreach (var property in properties)
            {
                if (!property.PropertyType.IsPrimitive &&
                    property.PropertyType != typeof(DateTime) &&
                    property.PropertyType != typeof(DateTime?) &&
                    property.PropertyType != typeof(string) &&
                    property.PropertyType != typeof(decimal) ||
                    property.Name == "Id")
                {
                    continue;
                }

                var typeConverter = TypeDescriptor.GetConverter(property.PropertyType);

                while (true)
                {
                    Program.MessageOutput(string.Format(EnterValueText, property.Name));

                    try
                    {
                        var value = Program.Input();

                        if (value.ToLower() == "r")
                        {
                            return null;
                        }

                        if (!isNew && string.IsNullOrWhiteSpace(value))
                        {
                            break;
                        }

                        foreach (var attribute in property.GetCustomAttributes())
                        {
                            if (attribute is RequiredAttribute && string.IsNullOrWhiteSpace(value))
                            {
                                throw new Exception(ValueCannotBeEmptyText);
                            }

                            if (attribute is MaxLengthAttribute attr && attr.Length < value.Length)
                            {
                                throw new Exception(string.Format(ValueCannotBeGreaterThanText, attr.Length));
                            }
                        }

                        property.SetValue(entity, typeConverter.ConvertFromString(value));
                        break;
                    }
                    catch (Exception e)
                    {
                        Program.ErrorOutput(string.Format(IncorrectInputMessageText, e.Message));
                    }
                }
            }

            return entity;
        }

        private void Add<TEntity>() where TEntity : class, new()
        {
            var controller = new Controller<TEntity>(_serviceProvider.GetService<IService<TEntity>>());
            var entity = FillEntity(new TEntity(), true);

            if (entity == null)
            {
                return;
            }

            controller.Add(entity);
            Program.ResultOutput(TextFormat<TEntity>(SuccessfullyAddedText));
        }

        private void Update<TEntity>() where TEntity : class, new()
        {
            var controller = new Controller<TEntity>(_serviceProvider.GetService<IService<TEntity>>());

            while (true)
            {
                Program.MessageOutput(TextFormat<TEntity>(EnterIdForUpdateText));

                var input = Program.Input();

                if (input.ToLower() == "r")
                {
                    return;
                }

                if (int.TryParse(input, out var id))
                {
                    var entity = controller.GetById(id);
                    if (entity == null)
                    {
                        Program.ErrorOutput(TextFormat<TEntity>(EntityNotExistsText));
                        continue;
                    }

                    OutputSingle(entity);

                    var changedEntity = FillEntity(entity, false);
                    if (changedEntity == null)
                    {
                        return;
                    }

                    controller.Update(changedEntity);
                    Program.ResultOutput(TextFormat<TEntity>(SuccessfullyUpdatedText));
                    return;
                }
                else
                {
                    Program.ErrorOutput(IncorrectInputText);
                }
            }
        }

        private void Remove<TEntity>() where TEntity : class, new()
        {
            var controller = new Controller<TEntity>(_serviceProvider.GetService<IService<TEntity>>());

            while (true)
            {
                Program.MessageOutput(TextFormat<TEntity>(EnterIdForRemoveText));

                var input = Program.Input();

                if (input.ToLower() == "r")
                {
                    return;
                }

                if (int.TryParse(input, out var id))
                {
                    var entity = controller.GetById(id);
                    if (entity == null)
                    {
                        Program.ErrorOutput(TextFormat<TEntity>(EntityNotExistsText));
                    }
                    else
                    {
                        controller.Remove(entity);
                        Program.ResultOutput(TextFormat<TEntity>(SuccessfullyRemovedText));
                        return;
                    }
                }
                else
                {
                    Program.ErrorOutput(IncorrectInputText);
                }
            }
        }
    }
}