﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Database.Migrations
{
    public partial class CreateSberbankDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "customers",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    first_name = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    middle_name = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    last_name = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    email = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    phone = table.Column<string>(type: "character varying(15)", maxLength: 15, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customers", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "accounts",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    customer_id = table.Column<int>(type: "integer", nullable: false),
                    account_number = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    opening_date = table.Column<DateTime>(type: "date", nullable: false),
                    closing_date = table.Column<DateTime>(type: "date", nullable: true),
                    balance = table.Column<decimal>(type: "money", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_accounts", x => x.id);
                    table.ForeignKey(
                        name: "FK_accounts_customers_customer_id",
                        column: x => x.customer_id,
                        principalTable: "customers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "cards",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    account_id = table.Column<int>(type: "integer", nullable: false),
                    card_number = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    start_date = table.Column<DateTime>(type: "date", nullable: false),
                    end_date = table.Column<DateTime>(type: "date", nullable: false),
                    is_blocked = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cards", x => x.id);
                    table.ForeignKey(
                        name: "FK_cards_accounts_account_id",
                        column: x => x.account_id,
                        principalTable: "accounts",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "customers",
                columns: new[] { "id", "email", "first_name", "last_name", "middle_name", "phone" },
                values: new object[,]
                {
                    { 1, "pushkin99@pochta.re", "Александр", "Пушкин", "Сергеевич", "+70001112233" },
                    { 2, "jukovskiy83@pochta.re", "Василий", "Жуковский", "Андреевич", "+74445556677" },
                    { 3, "lermontov14@pochta.re", "Михаил", "Лермонтов", "Юрьевич", "+78889990011" },
                    { 4, "krilov69@pochta.re", "Иван", "Крылов", "Андреевич", "+72223334455" },
                    { 5, "tutchev03@pochta.re", "Федор", "Тютчев", "Иванович", "+76667778899" }
                });

            migrationBuilder.InsertData(
                table: "accounts",
                columns: new[] { "id", "account_number", "balance", "closing_date", "customer_id", "opening_date" },
                values: new object[,]
                {
                    { 1, "11111111111111111111", 5000000m, new DateTime(1837, 1, 29, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, new DateTime(1799, 5, 26, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, "22222222222222222222", 4000000m, new DateTime(1852, 4, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, new DateTime(1783, 1, 29, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, "33333333333333333333", 3000000m, new DateTime(1841, 7, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, new DateTime(1814, 10, 3, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 4, "44444444444444444444", 2000000m, new DateTime(1844, 11, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), 4, new DateTime(1769, 2, 2, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 5, "55555555555555555555", 1000000m, new DateTime(1873, 7, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, new DateTime(1803, 11, 23, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "cards",
                columns: new[] { "id", "account_id", "card_number", "end_date", "is_blocked", "start_date" },
                values: new object[,]
                {
                    { 1, 1, "66666666666666666666", new DateTime(1838, 12, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), true, new DateTime(1836, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 2, "77777777777777777777", new DateTime(1852, 12, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), true, new DateTime(1850, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 3, "88888888888888888888", new DateTime(1842, 12, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), true, new DateTime(1840, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 4, 4, "99999999999999999999", new DateTime(1845, 12, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), true, new DateTime(1843, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 5, 5, "10101010101010101010", new DateTime(1874, 12, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), true, new DateTime(1872, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.CreateIndex(
                name: "IX_accounts_customer_id",
                table: "accounts",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "IX_cards_account_id",
                table: "cards",
                column: "account_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "cards");

            migrationBuilder.DropTable(
                name: "accounts");

            migrationBuilder.DropTable(
                name: "customers");
        }
    }
}
