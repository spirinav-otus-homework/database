DROP TABLE IF EXISTS customers CASCADE;
CREATE TABLE customers
(
	id SERIAL PRIMARY KEY,
	first_name VARCHAR(100) NOT NULL,
	middle_name VARCHAR(100) NULL,
	last_name VARCHAR(100) NOT NULL,
	email VARCHAR(50) NULL,
	phone VARCHAR(15) NOT NULL
);

DROP TABLE IF EXISTS accounts CASCADE;
CREATE TABLE accounts
(
	id SERIAL PRIMARY KEY,
	customer_id INTEGER,
	account_number VARCHAR(20) NOT NULL,
	opening_date DATE NOT NULL,
	closing_date DATE NULL,
	balance MONEY NOT NULL,
	FOREIGN KEY (customer_id) REFERENCES customers (Id) ON DELETE CASCADE
);

DROP TABLE IF EXISTS cards;
CREATE TABLE cards
(
	id SERIAL PRIMARY KEY,
	account_id INTEGER,
	card_number VARCHAR(20) NOT NULL,
	start_date DATE NOT NULL,
	end_date DATE NOT NULL,
	is_blocked BOOL NOT NULL,
	FOREIGN KEY (account_id) REFERENCES accounts (Id) ON DELETE CASCADE
);