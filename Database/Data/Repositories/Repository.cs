﻿using System.Linq;
using Database.Data.Models;

namespace Database.Data.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, new()
    {
        protected readonly SberbankDbContext DbContext;

        public Repository(SberbankDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public IQueryable<TEntity> GetAll()
        {
            return DbContext.Set<TEntity>();
        }

        public TEntity GetById(int id)
        {
            return DbContext.Set<TEntity>().Find(id);
        }

        public TEntity Add(TEntity entity)
        {
            DbContext.Add(entity);
            DbContext.SaveChanges();

            return entity;
        }

        public TEntity Update(TEntity entity)
        {
            DbContext.Update(entity);
            DbContext.SaveChanges();

            return entity;
        }

        public void Remove(TEntity entity)
        {
            DbContext.Remove(entity);
            DbContext.SaveChanges();
        }
    }
}