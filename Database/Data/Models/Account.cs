﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Database.Data.Models
{
    [Table("accounts")]
    public class Account
    {
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [Column("customer_id")]
        public int CustomerId { get; set; }

        public Customer Customer { get; set; }

        [Required]
        [MaxLength(20)]
        [Column("account_number")]
        public string AccountNumber { get; set; }

        [Required]
        [Column("opening_date", TypeName = "date")]
        public DateTime OpeningDate { get; set; }

        [Column("closing_date", TypeName = "date")]
        public DateTime? ClosingDate { get; set; }

        [Required]
        [Column("balance", TypeName = "money")]
        public decimal Balance { get; set; }

        public List<Card> Cards { get; set; }

        public override string ToString()
        {
            return $"Account Id: {Id}; " +
                   $"CustomerId: {CustomerId}; " +
                   $"AccountNumber: {AccountNumber}; " +
                   $"OpeningDate: {OpeningDate.Date:d}; " +
                   $"ClosingDate: {(ClosingDate.HasValue ? $"{ClosingDate.Value:d}" : "не указана")}; " +
                   $"Balance: {Balance}.";
        }
    }
}