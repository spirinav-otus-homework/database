﻿using Database.Helpers;
using Microsoft.EntityFrameworkCore;

namespace Database.Data.Models
{
    public class SberbankDbContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Card> Cards { get; set; }

        public SberbankDbContext(DbContextOptions<SberbankDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().HasData(InitialDataHelper.Customers);
            modelBuilder.Entity<Account>().HasData(InitialDataHelper.Accounts);
            modelBuilder.Entity<Card>().HasData(InitialDataHelper.Cards);
        }
    }
}