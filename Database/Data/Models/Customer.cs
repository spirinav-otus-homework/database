﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Database.Data.Models
{
    [Table("customers")]
    public class Customer
    {
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        [Column("first_name")]
        public string FirstName { get; set; }

        [MaxLength(100)]
        [Column("middle_name")]
        public string MiddleName { get; set; }

        [Required]
        [MaxLength(100)]
        [Column("last_name")]
        public string LastName { get; set; }

        [MaxLength(50)]
        [Column("email")]
        public string Email { get; set; }

        [Required]
        [MaxLength(15)]
        [Column("phone")]
        public string Phone { get; set; }

        public List<Account> Accounts { get; set; }

        public override string ToString()
        {
            return $"Customer Id: {Id}; " +
                   $"FirstName: {FirstName}; " +
                   $"MiddleName: {MiddleName}; " +
                   $"LastName: {LastName}; " +
                   $"Email: {Email}; " +
                   $"Phone: {Phone}.";
        }
    }
}