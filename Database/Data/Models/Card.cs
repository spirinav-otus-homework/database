﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Database.Data.Models
{
    [Table("cards")]
    public class Card
    {
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [Column("account_id")]
        public int AccountId { get; set; }

        public Account Account { get; set; }

        [Required]
        [MaxLength(20)]
        [Column("card_number")]
        public string CardNumber { get; set; }

        [Required]
        [Column("start_date", TypeName = "date")]
        public DateTime StartDate { get; set; }

        [Required]
        [Column("end_date", TypeName = "date")]
        public DateTime EndDate { get; set; }

        [Column("is_blocked")]
        public bool IsBlocked { get; set; }

        public override string ToString()
        {
            return $"Card Id: {Id}; " +
                   $"AccountId: {AccountId}; " +
                   $"CardNumber: {CardNumber}; " +
                   $"StartDate: {StartDate.Date:d}; " +
                   $"EndDate: {EndDate.Date:d}; " +
                   $"IsBlocked: {IsBlocked}.";
        }
    }
}