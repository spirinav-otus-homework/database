﻿using System;
using Database.Data.Models;

namespace Database.Helpers
{
    public static class InitialDataHelper
    {
        public static Customer[] Customers => new Customer[]
        {
            new()
            {
                Id = 1, FirstName = "Александр", MiddleName = "Сергеевич", LastName = "Пушкин",
                Email = "pushkin99@pochta.re", Phone = "+70001112233"
            },
            new()
            {
                Id = 2, FirstName = "Василий", MiddleName = "Андреевич", LastName = "Жуковский",
                Email = "jukovskiy83@pochta.re", Phone = "+74445556677"
            },
            new()
            {
                Id = 3, FirstName = "Михаил", MiddleName = "Юрьевич", LastName = "Лермонтов",
                Email = "lermontov14@pochta.re", Phone = "+78889990011"
            },
            new()
            {
                Id = 4, FirstName = "Иван", MiddleName = "Андреевич", LastName = "Крылов",
                Email = "krilov69@pochta.re", Phone = "+72223334455"
            },
            new()
            {
                Id = 5, FirstName = "Федор", MiddleName = "Иванович", LastName = "Тютчев",
                Email = "tutchev03@pochta.re", Phone = "+76667778899"
            }
        };

        public static Account[] Accounts => new Account[]
        {
            new()
            {
                Id = 1, CustomerId = 1, AccountNumber = "11111111111111111111",
                OpeningDate = new DateTime(1799, 5, 26), ClosingDate = new DateTime(1837, 1, 29), Balance = 5000000
            },
            new()
            {
                Id = 2, CustomerId = 2, AccountNumber = "22222222222222222222",
                OpeningDate = new DateTime(1783, 1, 29), ClosingDate = new DateTime(1852, 4, 12), Balance = 4000000
            },
            new()
            {
                Id = 3, CustomerId = 3, AccountNumber = "33333333333333333333",
                OpeningDate = new DateTime(1814, 10, 3), ClosingDate = new DateTime(1841, 07, 15), Balance = 3000000
            },
            new()
            {
                Id = 4, CustomerId = 4, AccountNumber = "44444444444444444444",
                OpeningDate = new DateTime(1769, 2, 2), ClosingDate = new DateTime(1844, 11, 9), Balance = 2000000
            },
            new()
            {
                Id = 5, CustomerId = 5, AccountNumber = "55555555555555555555",
                OpeningDate = new DateTime(1803, 11, 23), ClosingDate = new DateTime(1873, 07, 15),
                Balance = 1000000
            }
        };

        public static Card[] Cards => new Card[]
        {
            new()
            {
                Id = 1, AccountId = 1, CardNumber = "66666666666666666666", StartDate = new DateTime(1836, 1, 1),
                EndDate = new DateTime(1838, 12, 31), IsBlocked = true
            },
            new()
            {
                Id = 2, AccountId = 2, CardNumber = "77777777777777777777", StartDate = new DateTime(1850, 1, 1),
                EndDate = new DateTime(1852, 12, 31), IsBlocked = true
            },
            new()
            {
                Id = 3, AccountId = 3, CardNumber = "88888888888888888888", StartDate = new DateTime(1840, 1, 1),
                EndDate = new DateTime(1842, 12, 31), IsBlocked = true
            },
            new()
            {
                Id = 4, AccountId = 4, CardNumber = "99999999999999999999", StartDate = new DateTime(1843, 1, 1),
                EndDate = new DateTime(1845, 12, 31), IsBlocked = true
            },
            new()
            {
                Id = 5, AccountId = 5, CardNumber = "10101010101010101010", StartDate = new DateTime(1872, 1, 1),
                EndDate = new DateTime(1874, 12, 31), IsBlocked = true
            }
        };
    }
}