﻿using System.IO;
using Microsoft.Extensions.Configuration;

namespace Database.Helpers
{
    public static class ConfigHelper
    {
        public static string GetConnectionString()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true)
                .Build()
                .GetConnectionString("SberbankDbConnection");
        }
    }
}